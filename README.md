

# Video(Audio) Translation by AI 

[![简体中文 badge](https://img.shields.io/badge/%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87-Simplified%20Chinese-blue)](./README.md)

o3sky-VTAI V0.0.2


非常感谢您来到我的 **全自动视频翻译** 项目！该项目旨在提供一个简单易用的自动识别、翻译工具和其他视频辅助工具，帮助快速识别视频字幕、翻译字幕

#### 本项目开源可魔改，感谢支持！请勿在任何平台收费项目源码！

## 项目亮点
> *   支持 **OpenAI API** 和 **Faster-Whisper**、**Open-Whisper**、**SenseVoiceSmall** 识别后端。
> *   支持 **GPU 加速**、**VAD辅助**、**FFmpeg加速**。
> *   支持 **本地部署模型**、**ChatGPT**、**KIMI**、**DeepSeek**、**ChatGLM**、**Claude**等多种引擎翻译。
> *   支持识别、翻译 **多种语言** 和 **多种文件格式** 。
> *   支持对 **一键生成**、**字幕微调**、**视频预览**。
> *   支持对内容直接进行 **AI总结、问答**。

## 版本升级说明

1. v0.0.1 语音转文本模型使用开源的whisper模型，使用medium尺寸模型;大语言模型使用阿里Qwen2-7B-Instruct模型实现文本推理和翻译功能。
2. v0.0.2 升级模型语音转文本模型使用阿里SenseVoice模型,大语言模型切换到书生浦语internlm2_5-7b-chat模型实现文本推理和翻译功能。



## 技术架构

###      部署架构图

  本系统部署在云平台上，需要支持GPU或类似GPU加速推理的显卡等硬件资源支持，详细部署图见下图。

![image-20240916105340998](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/Obsidian/image-20240916105340998.png)

###   应用架构图

#### 媒体识别

​     用户上传音视频文件，系统将使用ffmpeg对上传文件进行流媒体处理（将不同音视频转换成统一格式音视频文件），然后程序会调用语音转文本模型，将音视频文件转化成文本信息，后面我们调用LLM 大语音模型对文本内容信息通过提示词要求让其转换对应的翻译内容，LLM模型收到消息进行处理，最后将处理的信息和ffmpeg在进行一次流媒体处理合成最后转换成翻译后的音视频文件。

  ![image-20240916111501584](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/Obsidian/image-20240916111501584.png)

#### 内容助手

   用户上传音视频文件，系统将使用ffmpeg对上传文件进行流媒体处理（将不同音视频转换成统一格式音视频文件），然后程序会调用语音转文本模型，将音视频文件转化成文本信息。用户根据音视频内容进行相关提问，问题会发送给LLM模型。 LLM模型会根据音视频内容和用户的问题进行回答。

![image-20240916112749338](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/Obsidian/image-20240916112749338.png)

#### 字幕翻译

   用户上传srt字幕文件，用户会根据自己需要翻译成中文、英文、其他国语言、双语等要求发送给系统，系统接受到信息发送给LLM大语音模型根据要求翻译成相应内容返回给用户。可以理解我们借助LLM大语言模型对文本内容进行翻译。

  ![image-20240916113244799](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/Obsidian/image-20240916113244799.png)

## 技术栈

   硬件： 显卡：

​               英伟达GPU 3060、4060、4070、4080、4090、A100、V100 、A800

​               天数智芯BI-V100 

​              CPU：12核心

​              内存：32GB

​              存储：60GB

  操作系统：Linux、widows

  开发语言：python

  主要依赖包：torch、torchaudio 、vllm（Linux平台）、streamlit、transformers、funasr

## 如何安装

### 使用 Windows

1. 安装 [Python](https://www.python.org/downloads/)，请确保Python版本大于3.8

2. 安装 [FFmpeg](https://www.ffmpeg.org/download.html)，[**Release**](https://github.com/Chenyme/Chenyme-AAVT/releases) 中`Full`版本已经打包了FFmpeg库

3. 运行 `install.bat`

4.  使用代码运行

   ```
   streamlit run app.py --server.port=7860
   ```

   

### 使用 docker

####      1 cpu 版本

   使用Dockerfile.cpu 编译打包镜像

```
docker build   -f Dockerfile.cpu . -t o3sky-vtat
```

   启动

```
docker run -it --name o3sky-vtat -p 7860:7860  o3sky-vtat:latest
```

####       2. GPU版本

   使用Dockerfile.gpu编译打包镜像

```
docker build   -f Dockerfile.gpu . -t o3sky-vtat
```

​    启动

```
docker run -it --name o3sky-vtat -p 7860:7860  o3sky-vtat:latest
```

####       3.gpgpu版本(天数智芯BI-V100)

 使用Dockerfile.gpgpu编译打包镜像， 这里我们使用了天数智芯定制软件镜像 作为模型的基础镜像

```
FROM registry.gitee-ai.local/base/iluvatar-corex:3.2.0-bi100
```



```
docker build   -f Dockerfile.gpgpu . -t o3sky-vtat-gpgpu
```

​     启动

```
docker run -it --name o3sky-vtat-gpgpu -p 7860:7860  o3sky-vtat-gpgpu:latest
```

### 在ai.gitee.com应用构建



## TODO![image-20240812132538455](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240812132538455.png)

构建完成后我们就可以直接看到访问页面了。

![image-20240913235027075](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/Obsidian/image-20240913235027075.png)



### 模型下载

### 1.使用git 下载

   进入jupyterlab页面

   ![image-20240812133327468](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240812133327468.png)

   后面我们通过以下命令行执行

  ~~whisper-medium模型~~

```
git clone https://gitee.com/hf-models/whisper-medium.git --depth=1 --single-branch --progress --verbose -b main  /home/app/model/whisper-medium
```

   SenseVoiceSmall模型

```
git clone https://gitee.com/hf-models/SenseVoiceSmall.git --depth=1 --single-branch --progress --verbose -b main  /home/app/model/SenseVoiceSmall
```

 ~~Qwen2-7B-Instruct模型~~

```
git clone https://gitee.com/hf-models/Qwen2-7B-Instruct.git --depth=1 --single-branch --progress --verbose -b main /home/app/model/Qwen2-7B-Instruct
```

internlm2_5-7b-chat模型

```
git clone https://gitee.com/hf-models/internlm2_5-7b-chat.git --depth=1 --single-branch --progress --verbose -b main /home/app/model/internlm2_5-7b-chat
```



#### 2.使用huggingface_hub 下载模型

```
from huggingface_hub import snapshot_download
snapshot_download(repo_id="hf-models/SenseVoiceSmall",local_dir="/home/app/model/SenseVoiceSmall")
```

![image-20240812133935224](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240812133935224.png)

internlm2_5-7b-chat模型

```
from huggingface_hub import snapshot_download
snapshot_download(repo_id="hf-models/internlm2_5-7b-chat",local_dir="/home/app/model/internlm2_5-7b-chat")
```

   ![image-20240812135245140](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240812135245140.png)

###  vllm 启动LLM模型推理

 单卡推理

```
python3 -m vllm.entrypoints.openai.api_server --model /home/app/model/internlm2_5-7b-chat --host 127.0.0.1 --port 8000 --trust-remote-code
```

 其中/home/app/model/internlm2_5-7b-chat 模型路径

双卡推理

```
python3 -m vllm.entrypoints.openai.api_server --model /data/model/internlm2_5-7b-chat --host 127.0.0.1 --port 8000 --trust-remote-code --tensor-parallel-size 2 
```

 其中/home/app/model/internlm2_5-7b-chat 模型路径

### 识别相关

- [x] 更换更快的SenseVoiceSmall项目
- [x] 支持本地模型加载
- [x] 支持个人微调SenseVoiceSmall模型
- [x] VAD辅助优化
- [x] 字词级断句优化
- [x] 更多的语种识别

### 翻译相关
- [x] 翻译优化
- [x] 更多的语种翻译
- [x] 更多的翻译模型
- [x] 更多的翻译引擎
- [x] 支持本地大语言模型翻译

### 字幕相关
- [x] 个性化字幕
- [x] 更多字幕格式
- [x] 字幕预览、实时修改
- [ ] 自动化字幕文本校对
- [ ] 双字幕

### 其他
- [x] 视频总结、罗列重点
- [x] 视频预览
- [x] AI助手
- [ ] 视频生成博客*
- [ ] 实时语音翻译
- [ ] 视频中文配音



## 项目界面预览

### 1.首页

![image-20240913162752221](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240913162752221.png)

 首页里面有个功能说明和 系统模型设置，打开设置，这里面我们列举了 模型配置、预置提示词、本地缓存 等相关设置

####  模型配置

  在模型配置里面我们可以设置本地模型以及其他第三方模型。

   本地设置，点击修改配置

  ![image-20240913162956480](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240913162956480.png)

   我们可以看到配置有3个参数

   API 地址，这里填写本地访问的模型URL，URL请求地址兼容openAI标准接口，默认情况不需要修改即可 http://127.0.0.1:8000/v1

   API密钥,  这里随便填写。因为我们使用本地模型，所以不需要key 可以保持默认即可。

   模型名称，这里填写模型在服务端部署模型路径，我们使用了书生浦语 internlm2_5-7b-chat模型。默认/data/model/internlm2_5-7b-chat ，也可以默认不需要修改。

 以上配置完成后本地模型配置就完成设置了。

 其他第三方模型配置，东西比较类似我们这里以硅基智能为案例给大家介绍

 点击修改配置

![image-20240814152159400](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814152159400.png)	

   API 地址，这里默认已经填写好硅基智能模型接口地址，默认是不需要修改的。

   API密钥，这里填写硅基智能第三方模型厂商提供的api秘钥。关于硅基智能模型申请可以访问[硅基智能](https://siliconflow.cn/)官网注册申请，目前该网站提供部分7B左右的小模型给大家测试使用，目前是免费使用的。可以放这个[地址](https://siliconflow.cn/zh-cn/maaspricing#siliconcloud)了解他们模型收费情况

 

![image-20240814152822466](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814152822466.png)

#### 预置提示词

 这里主要是使用翻译功能所以将翻译的提示词预设模式实现配置，默认提供2种翻译预设提示词

![image-20240814153033494](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814153033494.png)

#### 本地缓存

 这里主要记录视频、字幕、音频等上传临时存储的信息。大家根据自己的需要保留和删除。

![image-20240814153210962](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814153210962.png)

### 2.功能模块

​       这里是该系统主要功能区域，主要有3个功能模块。媒体识别、内容助手、字幕翻译 3个功能。

####    2.1媒体识别

#####      参数设置

​     识别设置这块我们使用本地SenseVoiceSmall模型音视频转文本模型，本地模型选择smalll模型(这个模型只有一个小模型开源)

![img](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_17262164132813.png)

​      翻译引擎

​       这里我们选择本地模型，当然你也可以根据自己的需要设置第三方LLM语言模型

​       ![image-20240814171401924](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814171401924.png)

​    下面的可以默认即可。

#####      音频识别功能

​     本功能可以通过上传音视频文件，通过**FFmpeg** 对音视频进行处理，提取音视频内容信息，然后后端自动调用本地LLM模型或者第三方模型实现语言翻译功能。

 ![image-20240814171726304](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814171726304.png)

 等待文件上传后，我们点击右边窗体中“音视频识别”

 ![image-20240814172741351](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814172741351.png)

##### 视频识别

​    本功能可以通过上传视频文件，通过**FFmpeg** 对视频进行处理，提取视频内容信息，然后后端自动调用本地LLM模型或者第三方模型实现语言翻译功，将翻译的文字在使用FFmpeg进行合成这样就可以实现无字幕视频打上有字幕视频，单语言视频转换成双语视频，自动配置字幕功能了。

   ![image-20240815094307129](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240815094307129.png)

 上传视频文件后，点击一键生成视频功能。后端程序对视频做视频处理，提取视频内容信息然后调用后端本地LLM模型或者使用第三方LLM模型将视频内容转换成文本，然后将转换文字在使用FFmpeg进行视频合成，这样一下就能转换成翻译后的视频了。（包含单字幕，双语字幕）

![image-20240815095944397](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240815095944397.png)

也可以分开实现，第一步生成字幕，然后调整生成的字幕在点击 合成字幕这样做出的字幕合成视频将更加友好。

 先生成字幕，然后更加内容可以手工调整。

![image-20240815101050735](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240815101050735.png)

![image-20240815101231773](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240815101231773.png)

![image-20240815101336906](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240815101336906.png)

#### 2.2内容助手

​     内容助手主要功能是用户通过上传音视频文件，后端程序使用**FFmpeg** 对音视频进行处理，提取音视频内容信息。然后使用本地LLM模型或者使用第三方LLM模型进行总结归纳 推理等功能。

#####      参数设置

​      这里我们使用SenseVoiceSmall模型音视频转文本模型。

​      SenseVoice模式 我们选择SenseVoiceSmall模型。

![image-20240913163605082](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240913163605082.png)

​        本地模型配置，我们从下拉列表中选择SenseVoiceSmall模型.其他可以保持默认，以上设置完成后点击保存参数设置。

​         ![image-20240814155933251](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814155933251.png)

#####            内容问答

​           这里面我们可以对上传的音视频文件进行解析，然后通过LLM大模型对识别的内容进行总结和推理。比如您参加一次培训课程，通过录音笔记录了培训的内容。我们就可以使用该功能实现音视频转录+音视频内容总结提炼等功能了。相当于阿里通义听悟功能（哈哈，当然是山寨版的）

​          ![image-20240814161244463](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814161244463.png)

   第一步，音频视频文件上传，参考上面步骤

   第二步，点击开始识别。这个时候后端程序会使用ffmpeg对视频文件进行处理，处理后调用SenseVoiceSmall模型音视频转文本模型，将音视频文件转换成文本内容。

 ![image-20240814161606590](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814161606590.png)

 ![image-20240814165914061](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814165914061.png)

视频解析完成后，我们就可以通过文本输入框使用llm语言模型进行推理了。

![image-20240814170954536](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/image-20240814170954536.png)

#### 2.3字幕翻译

​      点击字幕翻译，左小角有个SRT上传器，这里主要是通过上传字幕文件srt文件上传后，然后通过本地模型或则第三方LLM模型实现字幕自动翻译

​     ![企业微信截图_17236217486306](https://mypicture-1258720957.cos.ap-nanjing.myqcloud.com/%E4%BC%81%E4%B8%9A%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_17236217486306.png)
