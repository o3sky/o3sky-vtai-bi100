from faster_whisper import WhisperModel
import time
start_time = time.time()  # 记录开始时间
# 修改这一行
model = WhisperModel("E:\\AI\\faster-whisper-medium", device="cuda", compute_type="float16", local_files_only=True)
segments, info = model.transcribe("D:\\tmp\\output.mp3")
for segment in segments:
    print("[%.2fs -> %.2fs] %s" % (segment.start, segment.end, segment.text))
end_time = time.time()  # 记录结束时间
execution_time = end_time - start_time
print(f"- 方法执行时间：{execution_time:.2f} 秒")