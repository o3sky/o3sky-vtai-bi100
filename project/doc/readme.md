

# Video(Audio) Translation by AI 

[![简体中文 badge](https://img.shields.io/badge/%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87-Simplified%20Chinese-blue)](./README.md)

o3sky-VTAI V0.0.1


非常感谢您来到我的 **全自动视频翻译** 项目！该项目旨在提供一个简单易用的自动识别、翻译工具和其他视频辅助工具，帮助快速识别视频字幕、翻译字幕

#### 本项目开源可魔改，发布请保留原作者 @o3sky 和 项目链接，感谢支持！请勿在任何平台收费项目源码！

## 项目亮点
> *   支持 **OpenAI API** 和 **openai-whisper** 识别后端。
> *   支持 **GPU 加速**、**VAD辅助**、**FFmpeg加速**。
> *   支持 **本地部署模型**、**ChatGPT**、**KIMI**、**DeepSeek**、**ChatGLM**、**Claude**等多种引擎翻译。
> *   支持识别、翻译 **多种语言** 和 **多种文件格式** 。
> *   支持对 **一键生成**、**字幕微调**、**视频预览**。
> *   支持对内容直接进行 **AI总结、问答**。


## 如何安装
> **修复闪退.bat请在 出现闪退/报错OMP/报错NoneType 的情况下 再运行，不要直接运行！**

### 使用 Windows

1. 安装 [Python](https://www.python.org/downloads/)，请确保Python版本大于3.8

2. 安装 [FFmpeg](https://www.ffmpeg.org/download.html)，[**Release**](https://github.com/Chenyme/Chenyme-AAVT/releases) 中`Full`版本已经打包了FFmpeg库

3. 运行 `install.bat`

4.  使用代码运行

   ```
   streamlit run app.py --server.port=7860
   ```

   

### 使用 docker

####      1 cpu 版本

   使用Dockerfile.cpu 编译打包镜像

```
docker build   -f Dockerfile.cpu . -t o3sky-vtat
```

   启动

```
docker run -it --name o3sky-vtat -p 7860:7860  o3sky-vtat:latest
```

####       2. GPU版本

   使用Dockerfile.gpu编译打包镜像

```
docker build   -f Dockerfile.gpu . -t o3sky-vtat
```

​    启动

```
docker run -it --name o3sky-vtat -p 7860:7860  o3sky-vtat:latest
```

####       3.gpgpu版本(天数智芯BI-V100)

 使用Dockerfile.gpgpu编译打包镜像， 这里我们使用了天数智芯定制软件镜像 作为模型的基础镜像

```
FROM registry.gitee-ai.local/base/iluvatar-corex:3.2.0-bi100
```



```
docker build   -f Dockerfile.gpgpu . -t o3sky-vtat-gpgpu
```

​     启动

```
docker run -it --name o3sky-vtat-gpgpu -p 7860:7860  o3sky-vtat-gpgpu:latest
```



## TODO

### 识别相关
- [x] 更换更快的Whisper项目
- [x] 支持本地模型加载
- [x] 支持个人微调Whisper模型
- [x] VAD辅助优化
- [x] 字词级断句优化
- [x] 更多的语种识别

### 翻译相关
- [x] 翻译优化
- [x] 更多的语种翻译
- [x] 更多的翻译模型
- [x] 更多的翻译引擎
- [x] 支持本地大语言模型翻译

### 字幕相关
- [x] 个性化字幕
- [x] 更多字幕格式
- [x] 字幕预览、实时修改
- [ ] 自动化字幕文本校对
- [ ] 双字幕

### 其他
- [x] 视频总结、罗列重点
- [x] 视频预览
- [x] AI助手
- [ ] 视频生成博客*
- [ ] 实时语音翻译
- [ ] 视频中文配音



## 项目界面预览

#### 
