1
00:00:00,000 --> 00:00:01,900
注册, login, and recharge

2
00:00:02,033 --> 00:00:04,300
Click on the "Community Mirror" in the left navigation bar.

3
00:00:04,800 --> 00:00:07,066
Type SD into the search box

4
00:00:08,233 --> 00:00:10,500
Click the search button on the right


5
00:00:11,366 --> 00:00:13,633
Selected day Smart Chip SD mirror image

6
00:00:13,700 --> 00:00:15,966
The platform supports 3 billing methods.

7
00:00:15,966 --> 00:00:18,300
Included is pricing based on volume, week, and month.

8
00:00:18,300 --> 00:00:19,766
Create instance name

9
00:00:19,766 --> 00:00:21,900
**直接英文翻译中文结果：「直接随机英文名」**

10
00:00:21,900 --> 00:00:24,600
currently, only Jiangsu and Zhejiang are available for selection in the platform.

11
00:00:24,600 --> 00:00:27,166
Translated text: Due to this mirror that only supports Tianqing Chip.

12
00:00:27,166 --> 00:00:29,500
Spec Model We choose a single card.

13
00:00:29,500 --> 00:00:32,233
The default mirror selection is in USD derive

14
00:00:32,233 --> 00:00:34,966
Service port, the system will automatically fill in.

15
00:00:35,966 --> 00:00:38,733
Selective Service Port Commitment Agreement

16
00:00:38,733 --> 00:00:40,166
Click to create an instance.

17
00:00:41,233 --> 00:00:43,200
Open the container instance interface.

18
00:00:43,200 --> 00:00:48,666
Please wait for a moment, when the instance shows "Running", the instance creation is successful.

19
00:00:50,400 --> 00:00:51,533
click on Jupyter

20
00:00:52,500 --> 00:00:53,966
Enter the Jupyter interface

21
00:00:55,566 --> 00:01:00,800
We open the README.md file, which contains the CLI code for starting the SD model.

Translation: We open the README.md file, which contains the CLI code for starting the SD model.

22
00:01:00,900 --> 00:01:05,400
At this moment, this mirror image has integrated SDXL and SD3 models.

23
00:01:05,400 --> 00:01:09,033
Next, we will demonstrate the SD3 model to you. 

Please translate this sentence into English.

24
00:01:09,066 --> 00:01:12,600
We replicate the SD3 model's startup program command code.

25
00:01:12,733 --> 00:01:15,700
Run that command in the shell command-line window.

26
00:01:15,766 --> 00:01:18,733
python3 app-sd3.py --repo /root/fshare/models/Stability-AI/stable-diffusion-3-medium-diffusers/

27
00:01:19,733 --> 00:01:22,766
Translation result:
The program starts to load the model, please wait a moment.

28
00:01:23,633 --> 00:01:26,466
For saving of everyone's time, we play at 5 times the speed.


29
00:01:26,466 --> 00:01:31,300
After the command line appears URL and port, it indicates that the startup is completed.

30
00:01:31,333 --> 00:01:33,166
Return to container instance interface

31
00:01:33,166 --> 00:01:34,466
"Press the Application Service"

32
00:01:35,666 --> 00:01:37,566
Prompted application service has been launched

33
00:01:37,566 --> 00:01:38,666
Password was copied.

34
00:01:39,633 --> 00:01:41,500
After clicking "Open Link," the translation results are as follows: then click to open the link

35
00:01:41,500 --> 00:01:44,366
Popup and input username and password dialog

36
00:01:48,666 --> 00:01:49,633
Click to log in.

37
00:01:52,566 --> 00:01:55,433
Direct navigation based on programmatic prompts

38
00:01:56,066 --> 00:01:57,466
"Click the Run button"

39
00:01:57,466 --> 00:01:59,200
The English translation of "模型执行推理任务" is: "The model performs inference tasks."

40
00:01:59,200 --> 00:02:00,333
Please wait a moment

41
00:02:03,200 --> 00:02:07,966
We have already seen the image of an astronaut in the forest.

42
00:02:08,033 --> 00:02:10,700
"We'll be testing some customized prompt words."

43
00:02:10,700 --> 00:02:12,100
click the Run button.

44
00:02:14,300 --> 00:02:17,366
Translated result: Now there appears the adorable picture of cute kittens.

45
00:02:17,366 --> 00:02:19,200
Return to the container instance page.

46
00:02:19,200 --> 00:02:20,566
Click the Power Off button.

47
00:02:20,566 --> 00:02:22,700
"Click OK to shut down."

48
00:02:22,700 --> 00:02:25,066
Platform also provides scheduled power-off function.

49
00:02:25,066 --> 00:02:26,833
Click on the scheduled shutdown button

50
00:02:26,833 --> 00:02:28,266
Choose the shutdown time

51
00:02:28,300 --> 00:02:31,266
Here, select the shutdown time according to your own needs.

52
00:02:34,533 --> 00:02:38,500
Click on the "OK" button, submitting the command execution operation for power-saving.

53
00:02:38,500 --> 00:02:41,466
Translation Result: An example will show the preset shutdown time.

54
00:02:41,466 --> 00:02:44,833
That's all about the video; thanks for watching.

55
00:01:45,666 --> 00:01:48,700
Enter your username and password on this page

56
00:01:50,000 --> 00:01:52,500
Enter the SD3 model inference page

