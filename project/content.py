import os
import toml
import time
import torch
import datetime
import streamlit as st
import streamlit_antd_components as sac
from openai import OpenAI
import whisper
import subprocess
from multiprocessing import Process, Manager
from .utils.utils2 import (transcribe_senseVoice_audio)
from modelscope.utils.logger import get_logger
import logging
logger = get_logger(log_level=logging.INFO)
logger.setLevel(logging.INFO)

def content():
    project_dir = os.path.dirname(os.path.abspath(__file__)).replace("\\", "/")
    config_dir = project_dir + "/config/"  # 配置文件
    cache_dir = project_dir + "/cache/"  # 本地缓存
    #model_dir = project_dir.replace("project", "model")
    model_dir = "/data/model"
    doc_dir = project_dir + "/doc/"  # 知识库

    api_config = toml.load(config_dir + "api.toml")  # 加载配置
    gemini_key = api_config["GEMINI"]["gemini_key"]  # GEMINI
    gemini_base = api_config["GEMINI"]["gemini_base"]
    ai01_key = api_config["AI01"]["AI01_key"]  # 01
    ai01_base = api_config["AI01"]["AI01_base"]
    kimi_key = api_config["KIMI"]["kimi_key"]  # kimi
    kimi_base = api_config["KIMI"]["kimi_base"]
    chatglm_key = api_config["CHATGLM"]["chatglm_key"]  # chatglm
    chatglm_base = api_config["CHATGLM"]["chatglm_base"]
    openai_key = api_config["GPT"]["openai_key"]  # openai
    openai_base = api_config["GPT"]["openai_base"]
    claude_key = api_config["CLAUDE"]["claude_key"]  # claude
    claude_base = api_config["CLAUDE"]["claude_base"]
    deepseek_key = api_config["DEEPSEEK"]["deepseek_key"]  # deepseek
    deepseek_base = api_config["DEEPSEEK"]["deepseek_base"]
    local_key = api_config["LOCAL"]["api_key"]  # local
    local_base = api_config["LOCAL"]["base_url"]
    local_model = api_config["LOCAL"]["model_name"]

    siliconflow_key = api_config["SILICONFLOW"]["siliconflow_key"]  # siliconflow
    siliconflow_base = api_config["SILICONFLOW"]["siliconflow_base"]

    content_config = toml.load(config_dir + "content.toml")  # 加载video配置
    #openai_whisper_api = content_config["WHISPER"]["openai_whisper_api"]  # openai_whisper配置
    # openai_whisper_model = content_config["WHISPER"]["openai_whisper_model_default"]  # openai_whisper配置
    # openai_whisper_local = content_config["WHISPER"]["openai_whisper_model_local"]  # 本地模型加载
    # openai_whisper_local_path = content_config["WHISPER"]["openai_whisper_model_local_path"]  # 本地模型路径
    sensevoice_api = content_config["SENSEVOICE"]["sensevoice_api"]  # openai_whisper配置
    sensevoice_url=content_config["SENSEVOICE"]["sensevoice_url"] #sensevoice模型URL地址
    sensevoice_model_local = content_config["SENSEVOICE"]["sensevoice_model_local"]  # 本地模型加载
    sensevoice_model = content_config["SENSEVOICE"]["sensevoice_model_default"]  # sensevoice配置模型类型
    sensevoice_model_local_path=content_config["SENSEVOICE"]["sensevoice_model_local_path"] #sensevoice本地模型路径
    gpu_setting = content_config["SENSEVOICE"]["gpu"]
    beam_size_setting = content_config["MORE"]["beam_size"]
    temperature_setting = content_config["MORE"]["temperature"]
    log_setting = content_config["MORE"]["log"]

    # options = {'openai-whisper': {'models': {'tiny': 0, 'tiny.en': 1, 'base': 2, 'base.en': 3, 'small': 4,
    #                                          'small.en': 5, 'medium': 6, 'medium.en': 7, 'large-v1': 8,
    #                                          'large-v2': 9, 'large-v3': 10, 'large': 11}}}

    options = {'sensevoice': {'models': {'samall': 0}}}
    st.subheader("AI 内容问答助手")
    st.caption("AI Content Q&A Assistant")
    session_stateflag=False

    with st.sidebar:
        st.write("#### 文件上传器")
        uploaded_file = st.file_uploader("请在这里上传文件：", type=["mp3", "mpga", "m4a", "wav", 'mp4', 'mov', 'avi', 'm4v', 'webm', 'flv', 'ico'], label_visibility="collapsed")
        # 检查文件是否已上传
        if uploaded_file is not None:
            st.success(' 文件已成功上传')
        st.write("#### 音频视频示例")
        try:
            with open(doc_dir + "audio.mp3", 'rb') as file:
                doc = file.read()
            st.download_button(
                label="下载音频视频样例文件",
                data=doc,
                key='str_download',
                file_name='声音.mp3',
                mime='text/str',
                use_container_width=True
            )
            # if download_file is not None:
            #     st.toast("下载音频视频文件成功", icon=":material/task_alt:")
        except:
            st.toast("未检测到文件", icon=":material/error:")
    name = sac.segmented(
        items=[
            sac.SegmentedItem(label="参数设置", icon="gear-wide-connected"),
            sac.SegmentedItem(label="内容问答", icon="file-check"),
            sac.SegmentedItem(label="gitee", icon="gitee", href="https://gitee.com/o3sky/o3sky-vtai-bi100"),
        ], align='center', size='sm', radius=20, color='red', divider=False, use_container_width=True
    )

    if name == "参数设置":
        col1, col2 = st.columns([0.7, 0.3], gap="medium")
        with col1:
            with st.expander("**识别设置**", expanded=True):
                model = st.selectbox("SenseVoice模式", ("OpenAI-API 接口调用", "SenseVoice 本地部署"), index=0 if sensevoice_api else 1, help="`OpenAI-API 接口调用`：使用OpenAI的官方接口进行识别，文件限制25MB（不是上传视频文件，是该项目转换后的音频文件，可以前往Cache查看每次的大小），过大会导致上传失败\n\n`sensevoice 本地部署`：本地识别字幕，无需担心大小限制。请注意，若网络不佳请启用下方的本地模型加载")
                if model == "OpenAI-API 接口调用":
                    sensevoice_api = True
                    content_config["SENSEVOICE"]["sensevoice_api"] = sensevoice_api
                    local_on = False
                    content_config["SENSEVOICE"]["sensevoice_model_local"] = local_on

                else:
                    col3, col4 = st.columns(2, gap="small")
                    sensevoice_api = False
                    content_config["SENSEVOICE"]["sensevoice_api"] = sensevoice_api
                    with col3:
                        local_on = st.checkbox('本地模型', sensevoice_model_local, help="使用本地下载好的模型进行转录")
                    content_config["SENSEVOICE"]["sensevoice_model_local"] = local_on

                if not sensevoice_api:
                    with col4:
                        gpu = st.checkbox('GPU加速', disabled=not torch.cuda.is_available(), help='cuda、pytorch正确后才可使用！', value=gpu_setting)
                        content_config["SENSEVOICE"]["gpu"] = gpu

                    if local_on:
                        model_names = os.listdir(model_dir)
                        path = sensevoice_model_local_path

                        try:
                            index_model = model_names.index(path.replace(model_dir + '/', ''))
                            local_option = st.selectbox('本地模型', model_names, index=index_model, help="模型下载：https://ai.gitee.com/hf-models/SenseVoiceSmall.git")
                        except:
                            local_option = st.selectbox('本地模型', model_names, index=0, help="模型下载：https://ai.gitee.com/hf-models/SenseVoiceSmall.git")
                        local_model_path = model_dir + '/' + local_option
                        content_config["SENSEVOICE"]["sensevoice_model_local_path"] = local_model_path

                    elif not local_on:
                        model_option = st.selectbox('识别模型', list(options['sensevoice']['models'].keys()), index=options['sensevoice']['models'][sensevoice_model], help="推荐large模型")
                        content_config["SENSEVOICE"]["sensevoice_model_default"] = model_option

            with st.expander("**高级设置**", expanded=True):
                beam_size = st.number_input("束搜索大小", min_value=1, max_value=20, value=beam_size_setting, step=1, disabled=sensevoice_api, help="`beam_size`参数。用于定义束搜索算法中每个时间步保留的候选项数量。束搜索算法通过在每个时间步选择最有可能的候选项来构建搜索树，并根据候选项的得分进行排序和剪枝。较大的beam_size值会保留更多的候选项，扩大搜索空间，可能提高生成结果的准确性，但也会增加计算开销。相反，较小的beam_size值会减少计算开销，但可能导致搜索过早地放弃最佳序列。")
                temperature = st.number_input("sensevoice温度", min_value=0.0, max_value=1.0, value=temperature_setting, step=0.1, help="sensevoice转录时模型温度，越大随机性（创造性）越高。")
                log = st.selectbox("FFmpeg-日志级别", ["quiet", "panic", "fatal", "error", "warning", "info", "verbose", "debug", "trace"], index=["quiet", "panic", "fatal", "error", "warning", "info", "verbose", "debug", "trace"].index(log_setting), help="FFmpeg输出日志。\n- **quiet**：没有输出日志。\n- **panic**：仅在不可恢复的致命错误发生时输出日志。\n- **fatal**：仅在致命错误发生时输出日志。\n- **error**：在错误发生时输出日志。\n- **warning**：在警告级别及以上的事件发生时输出日志。\n- **info**：在信息级别及以上的事件发生时输出日志。\n- **verbose**：输出详细信息，包括调试和信息级别的日志。\n- **debug**：输出调试信息，非常详细的日志输出。\n- **trace**：最详细的日志输出，用于极其详细的调试。")

                content_config["MORE"]["beam_size"] = beam_size
                content_config["MORE"]["temperature"] = temperature
                content_config["MORE"]["log"] = log

        with col2:
            if st.button("保存所有参数", type="primary", use_container_width=True):
                sac.divider(label='**参数提示**', icon='activity', align='center', color='gray')
                with open(config_dir + '/content.toml', 'w', encoding='utf-8') as file:
                    toml.dump(content_config, file)
                st.success("""
                **参数设置 已保存**
                ###### 所有参数设置已成功保存！""", icon=":material/check:")
            else:
                sac.divider(label='**参数提示**', icon='activity', align='center', color='gray')
                st.error("""
                **参数设置 未保存**
                ###### 参数设置尚未保存，请及时保存！""", icon=":material/close:")

            if check_ffmpeg():
                if check_cuda_support():
                    st.success("""
                    **FFmpeg GPU加速正常**
                    ###### 本次 FFmpeg 合并将使用 GPU 加速！""", icon=":material/check:")
                else:
                    st.warning("""
                    **FFmpeg 正常，但 GPU 加速失败**
                    ###### FFmpeg hwaccels 失败！（可忽略）""", icon=":material/warning:")
            else:
                st.error("""
                **FFmpeg 状态错误**
                ###### 未检测到 FFmpeg，请确认！""", icon=":material/close:")

            if sensevoice_api:
                st.warning("""
                **OpenAI API调用 识别 已开启**
                ###### 请确保 OPENAI 相关参数设置不为空！""", icon=":material/warning:")

            if not sensevoice_api:
                if gpu:
                    if torch.cuda.is_available():
                        st.success("""
                        **GPU加速模式 已开启**
                        ###### 本次识别将使用 GPU 加速模式""", icon=":material/check:")
                    else:
                        st.error("""
                        **GPU加速模式 未开启**
                        ###### 未检测到CUDA，请关闭 GPU 加速！""", icon=":material/close:")
                if local_on:
                    st.success("""
                    **本地模型识别 已开启**
                    ###### [模型下载](https://gitee.com/hf-models/SenseVoiceSmall.git) | [使用文档](""", icon=":material/check:")

            if not torch.cuda.is_available():
                st.error("""
                **CUDA 状态错误**
                ###### 未检测到CUDA，CPU 用户请忽略！""", icon=":material/close:")

            sac.divider(label='POWERED BY @o3sky', icon="lightning-charge", align='center', color='gray', key="1")

    if name == "内容问答":
        col1, col2 = st.columns([0.7, 0.3])
        with col2:
            if st.button("开始识别", type="primary", use_container_width=True):
                if uploaded_file is not None:
                    st.session_state.video_name = "uploaded." + uploaded_file.name.split('.')[-1]
                    time1 = time.time()
                    st.toast('已开始生成，请不要在运行时切换菜单或修改参数!', icon=":material/person_alert:")
                    msg = st.toast('正在进行视频提取', icon=":material/play_circle:")

                    current_time = datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S")
                    output_file = cache_dir + current_time
                    os.makedirs(output_file)
                    with open(output_file + '/' + st.session_state.video_name, "wb") as file:
                        file.write(uploaded_file.getbuffer())
                    print(f"- 本次任务目录：{output_file}")
                    #if uploaded_file.name.split(".")[-1] != "mp3":
                    file_to_mp3(log_setting, st.session_state.video_name, output_file)

                    time2 = time.time()
                    msg.toast('正在识别视频内容', icon=":material/hearing:")
                    if sensevoice_api:
                        result = openai_whisper_result(openai_key, openai_base, output_file, "Don’t make each line too long.", temperature_setting)
                    else:
                        device = 'cuda' if gpu_setting else 'cpu'
                        model = sensevoice_model
                        if sensevoice_model_local:
                            model = sensevoice_model_local_path
                        autofile=output_file+'/'+"output.mp3"
                        #result = openai_whisper_model_result(audio_file, device, openai_whisper_model,openai_whisper_local_path, "Don’t make each line too long.", temperature_setting, False, "自动识别", beam_size_setting, 500)
                        result=transcribe_senseVoice_audio(sensevoice_url,autofile,device,sensevoice_model_local_path,"fsmn-vad")
                        print(f"- 识别结果：{result}")
                    time3 = time.time()
                    total_time = time3 - time1
                    st.session_state.time = f"{total_time:.2f}"
                    st.session_state.text = result
                    session_stateflag = True
                    st.toast("已识别完成，开始对话叭！", icon=":material/task_alt:")
                else:
                    st.toast("未检测到文件", icon=":material/error:")

            with st.container():
                translate_option = sac.menu([
                    sac.MenuItem('local', icon='house-up-fill'),
                    sac.MenuItem('Moonshot-月之暗面', icon='node-plus-fill', children=[
                        sac.MenuItem('moonshot-v1-8k', icon='robot'),
                        sac.MenuItem('moonshot-v1-32k', icon='robot'),
                        sac.MenuItem('moonshot-v1-128k', icon='robot')
                    ]),
                    sac.MenuItem('ChatGLM-智谱AI', icon='node-plus-fill', children=[
                        sac.MenuItem('glm-4', icon='robot'),
                        sac.MenuItem('glm-4-0520', icon='robot'),
                        sac.MenuItem('glm-4-flash', icon='robot'),
                        sac.MenuItem('glm-4-air', icon='robot'),
                        sac.MenuItem('glm-4-airx', icon='robot')
                    ]),
                    sac.MenuItem('ChatGPT-OpenAI', icon='node-plus-fill', children=[
                        sac.MenuItem('gpt-4o-mini', icon='robot'),
                        sac.MenuItem('gpt-4', icon='robot'),
                        sac.MenuItem('gpt-4-turbo', icon='robot'),
                        sac.MenuItem('gpt-4o', icon='robot')
                    ]),
                    sac.MenuItem('DeepSeek-深度求索', icon='node-plus-fill', children=[
                        sac.MenuItem('deepseek-chat', icon='robot'),
                        sac.MenuItem('deepseek-coder', icon='robot')
                    ]),
                    sac.MenuItem('01AI-零一万物', icon='node-plus-fill', children=[
                        sac.MenuItem('yi-spark', icon='robot'),
                        sac.MenuItem('yi-medium', icon='robot'),
                        sac.MenuItem('yi-medium-200k', icon='robot'),
                        sac.MenuItem('yi-vision', icon='robot'),
                        sac.MenuItem('yi-large', icon='robot'),
                        sac.MenuItem('yi-large-rag', icon='robot'),
                        sac.MenuItem('yi-large-turbo', icon='robot'),
                        sac.MenuItem('yi-large-preview', icon='robot')
                    ]),
                    sac.MenuItem('siliconflow-硅基智能', icon='node-plus-fill', children=[
                        sac.MenuItem('01-ai/Yi-1.5-6B-Chat', icon='robot'),
                        sac.MenuItem('01-ai/Yi-1.5-9B-Chat-16K', icon='robot'),
                        #sac.MenuItem('zhipuai/chatglm3-6b', icon='robot'),
                        #sac.MenuItem('zhipuai/glm-4-9b-chat', icon='robot'),
                        sac.MenuItem('Qwen/Qwen1.5-7B-Chat', icon='robot'),
                        sac.MenuItem('Qwen/Qwen2-1.5B-Instruct', icon='robot'),
                        sac.MenuItem('Qwen/Qwen2-7B-Instruct', icon='robot'),
                        #sac.MenuItem('google/gemma-2-9b-it', icon='robot'),
                        #sac.MenuItem('mistralai/Mistral-7B-Instruct-v0.2', icon='robot'),
                        sac.MenuItem('internlm/internlm2_5-7b-chat', icon='robot'),
                        #sac.MenuItem('meta-llama/Meta-Llama-3-8B-Instruct', icon='robot'),
                        #sac.MenuItem('meta-llama/Meta-Llama-3.1-8B-Instruct', icon='robot'),
                    ]),
                ], height=650, size='sm', indent=20, open_all=True)

        with col1:
            with st.popover("**对话设置**", use_container_width=True):
                height = st.number_input("对话框高度", min_value=300, step=100, value=590)
                pre_prompt = st.text_input("Prompt", value="你是基于以下内容的BOT,请结合自身知识和内容回答用户问题，内容：\n")
                temperature_setting = st.number_input("Temperature", min_value=0.0, max_value=1., step=0.1, value=0.80)
            try:
                able = False if st.session_state.text else True
            except:
                able = True

            messages = st.container(height=height)
            if "messages1" not in st.session_state:
                st.session_state["messages1"] = [{"role": "assistant", "content": "您对上传的内容有什么疑问?"}]

            for msg1 in st.session_state.messages1:
                messages.chat_message(msg1["role"]).write(msg1["content"])

            if prompt := st.chat_input(disabled=able, placeholder="基于上传文件的的Chat，您可以问任何关于上传文件的问题"):
                time10 = time.time()
                st.session_state.messages1.append({"role": "user", "content": prompt})
                messages.chat_message("user").write(prompt)

                if "gpt" in translate_option:
                    client = OpenAI(api_key=openai_key, base_url=openai_base)
                elif "moonshot" in translate_option:
                    print("moonshot：：：：："+translate_option)
                    client = OpenAI(api_key=kimi_key, base_url=kimi_base)
                elif "glm" in translate_option:
                    client = OpenAI(api_key=chatglm_key, base_url=chatglm_base)
                elif "deepseek" in translate_option:
                    client = OpenAI(api_key=deepseek_key, base_url=deepseek_base)
                elif "yi" in translate_option:
                    client = OpenAI(api_key=ai01_key, base_url=ai01_base)
                elif "internlm" in translate_option or '01-ai' in translate_option or 'zhipuai' in translate_option or 'Qwen' in translate_option or 'mistralai' in translate_option or 'google' in translate_option or 'meta-llama' in translate_option:
                    print("siliconflow：：：：："+translate_option)
                    client = OpenAI(api_key=siliconflow_key, base_url=siliconflow_base)
                elif "local" in translate_option:
                    translate_option = local_model
                    client = OpenAI(api_key=local_key, base_url=local_base)
                print(translate_option)
                logger.info("-----------  translate_option -----------"+translate_option)
                response = client.chat.completions.create(model=translate_option,
                                                          messages=[{"role": "system", "content": pre_prompt + st.session_state.text},
                                                                    {"role": "user", "content": prompt}],
                                                          temperature=temperature_setting)
                msg1 = response.choices[0].message.content
                st.session_state.messages1.append({"role": "assistant", "content": msg1})
                messages.chat_message("assistant").write(msg1)
                time11 = time.time()
                total_time = time11 - time10
                st.session_state.time = f"{total_time:.2f}"
                session_stateflag = True
        try:
            if session_stateflag:
                sac.alert(
                    label=f'总耗时：{st.session_state.time}s',
                    size='lg', radius=20, icon=True, closable=True, color='success')
        except:
            test = 1
        sac.divider(label='POWERED BY @o3sky', icon="lightning-charge", align='center', color='gray', key="4")


# def runWhisperSeperateProc(*args):
#     print("\n*** openai Whisper 多进程调用中 ***")
#     print("- 进程启动中，出现 Warning streamlit.runtime.state.session_state_proxy... 属于正常现象，可忽略\n")
#     with Manager() as manager:
#         returnList = manager.list([None])
#         p = Process(target=openai_whisper_model_result, args=args + (returnList,))
#         p.start()
#         p.join()
#         p.close()
#         print("\n- 进程已结束，结果已返回！\n")
#         return returnList[0]

# def openai_whisper_model_result(file_path, device, model_name,model_path,prompt, temp, vad, lang, beam_size, min_vad):
#     start_time = time.time()  # 记录开始时间
#     if model_name not in ['tiny', 'tiny.en', 'base', 'base.en', 'small', 'small.en', 'medium', 'medium.en', 'large-v1',
#                           'large-v2', 'large-v3', 'large']:
#         print("\n*** Faster Whisper 本地模型加载模式 ***\n")
#     else:
#         print("\n*** Faster Whisper 调用模式 ***\n")
#     print(f"- 运行模型：{model_name}")
#     print(f"- 运行模型路径：{model_path}")
#     print(f"- 运行方式：{device}")
#     print(f"- VAD辅助：{vad}")
#
#     try:
#         file_path.split('.')
#         file_path = open(file_path, "rb")
#     except:
#         file_path = open(file_path + "/output.mp3", "rb")
#     # 提取文件路径
#     file_path_str = file_path.name
#     print(f"- file_path：{file_path_str}")
#     model = whisper.load_model(model_name,device,model_path)
#     print(f"- 加载whisper模型：")
#     result = model.transcribe(file_path_str)
#     print(f"- 模型whisper转成语言识别文字部分：")
#     segments = result["segments"]
#     segments_dict =openai_whisper_result_dict(segments)
#     print(f"- whisper识别内容：\n{segments_dict['text']}\n")
#     end_time = time.time()  # 记录结束时间
#     execution_time = end_time - start_time
#     print(f"- 方法执行时间：{execution_time:.2f} 秒")
#     #returnList[0] = segments_dict
#     return segments_dict
#
# def openai_whisper_result_dict(segments):
#     segments_dict = {
#         'text': ' '.join([segment['text'] for segment in segments]),
#         'segments': [{
#             'id': segment['id'],
#             'seek': segment['seek'],
#             'start': segment['start'],
#             'end': segment['end'],
#             'text': segment['text'],
#             'tokens': segment['tokens'],
#             'temperature': segment.get('temperature', None),
#             'avg_logprob': segment['avg_logprob'],
#             'compression_ratio': segment['compression_ratio'],
#             'no_speech_prob': segment['no_speech_prob']}
#             for segment in segments
#         ]
#     }
#     return segments_dict

def file_to_mp3(log, file_name, path):
    try:
        if file_name.split('.')[-1] != "mp3":
            command = f"ffmpeg -loglevel {log} -i {file_name} -vn -acodec libmp3lame -ab 320k -f mp3 output.mp3"
            subprocess.run(command, shell=True, cwd=path)
    except:
        raise EOFError("错误！可能是 FFmpeg 未被正确配置 或 上传文件格式不受支持！")
def check_ffmpeg():
    try:
        result = subprocess.run(['ffmpeg', '-version'], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if result.returncode == 0:
            return True
        else:
            return False
    except:
        return False
def check_cuda_support():
    try:
        result = subprocess.run(["ffmpeg", "-hwaccels"], capture_output=True, text=True)
        if torch.cuda.is_available() and "cuda" in result.stdout:
            return True
    except Exception as e:
        print(f" 未检测到 CUDA 状态，本地合并为 CPU 模式，若要使用 GPU 请检查 CUDA 是否配置成功")
        return False

def openai_whisper_result(key, base, path, prompt, temperature):
    print("\n*** OpenAI API 调用模式 ***\n")
    if base != "https://api.openai.com/v1":
        print(f"- 代理已开启，URL：{base}")
    try:
        path.split('.')
        audio_file = open(path, "rb")
    except:
        audio_file = open(path + "/output.mp3", "rb")

    client = OpenAI(api_key=key, base_url=base)
    transcript = client.audio.transcriptions.create(
        model="whisper-1",
        file=audio_file,
        response_format="verbose_json",
        timestamp_granularities=["segment"],
        prompt=prompt,
        temperature=temperature)
    result = {'text': transcript.text, 'segments': transcript.segments}
    print(f"- whisper识别内容：\n{result['text']}\n")
    return result