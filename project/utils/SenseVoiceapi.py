from fastapi import FastAPI
from pydantic import BaseModel
from funasr import AutoModel
from funasr.utils.postprocess_utils import rich_transcription_postprocess
import uvicorn
import soundfile as sf
import os

app = FastAPI()
class AudioInput(BaseModel):
    file_path: str
    device: str
    model_dir: str
    vad_model: str

model = None

def load_model(model_dir, vad_model, device):
    global model
    model = AutoModel(
        model=model_dir,
        vad_model=vad_model,
        vad_kwargs={"max_single_segment_time": 30000},
        device=device,
        hub="hf",
    )

def process_vad(audio_file_path, vad_model_dir):
    # 加载VAD模型
    vad_model = AutoModel(
        model=vad_model_dir,
        trust_remote_code=True,
        remote_code="./model.py",
        device="cuda:0",
        disable_update=True
    )

    # 使用VAD模型处理音频文件
    vad_res = vad_model.generate(
        input=audio_file_path,
        cache={},
        max_single_segment_time=30000  # 最大单个片段时长
    )

    # 从VAD模型的输出中提取每个语音片段的开始和结束时间
    segments = vad_res[0]['value']  # 假设只有一段音频，且其片段信息存储在第一个元素中

    return segments

# 定义一个函数来裁剪音频
def crop_audio(audio_data, start_time, end_time, sample_rate):
    start_sample = int(start_time * sample_rate / 1000)  # 转换为样本数
    end_sample = int(end_time * sample_rate / 1000)  # 转换为样本数
    return audio_data[start_sample:end_sample]


@app.get("/transcribes")
def process_sensevoice(audio_input: AudioInput):
    if model is None:
        load_model(audio_input.model_dir, audio_input.vad_model, audio_input.device)
    # 调用函数
    segments = process_vad(audio_input.file_path, audio_input.vad_model)
    # 加载原始音频数据
    audio_data, sample_rate = sf.read(audio_input.file_path)
        # 对每个语音片段进行处理
    results = []
    for segment in segments:
        start_time, end_time = segment  # 获取开始和结束时间
        cropped_audio = crop_audio(audio_data, start_time, end_time, sample_rate)

        # 将裁剪后的音频保存为临时文件
        directory = os.path.dirname(audio_input.file_path)
        temp_audio_file = directory+"/"+"temp_cropped.wav"
        sf.write(temp_audio_file, cropped_audio, sample_rate)

        # 语音转文字处理
        res = model.generate(
            input=temp_audio_file,
            cache={},
            language="auto",  # 自动检测语言
            use_itn=True,
            batch_size_s=60,
            merge_vad=True,  # 启用 VAD 断句
            merge_length_s=10000  # 合并长度，单位为毫秒
        )
        # 处理输出结果
        text = rich_transcription_postprocess(res[0]["text"])
        # 添加时间戳
        results.append({"start": start_time // 1000, "end": end_time // 1000, "text": text})  # 转换为秒

    return results
@app.get("/transcribe")
def transcribe(audio_input: AudioInput):
    if model is None:
        load_model(audio_input.model_dir, audio_input.vad_model, audio_input.device)
    # en
    res = model.generate(
        input=audio_input.file_path,
        cache={},
        language="auto",  # "zn", "en", "yue", "ja", "ko", "nospeech"
        use_itn=False,
        batch_size_s=60,
        merge_vad=True,  #
        merge_length_s=15,
    )
    text = rich_transcription_postprocess(res[0]["text"])
    return text

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8001)