import streamlit as st
import streamlit_antd_components as sac
from project.media import media
from project.home import home
from project.content import content
from project.translation import translation
from huggingface_hub import snapshot_download
from project.utils.utils2 import run_vllm_model
import subprocess

st.set_page_config(
    page_title="o3sky-vtai-bi100 v0.0.2",
    page_icon=":material/radio_button_checked:",
    layout="wide",
    initial_sidebar_state="expanded"
)

# 定义正确的密码
CORRECT_PASSWORD = "zhouhuizhou"

# JupyterLab URL
#JUPYTER_LAB_URL = "http://localhost:7861"  # 替换为您的 JupyterLab URL

with st.sidebar.container():
    st.subheader("o3sky-vtai-bi100")
    menu = sac.menu(
        items=[
            sac.MenuItem('主页', icon='house-fill'),
            sac.MenuItem('功能模块', icon='box-fill', children=[
                sac.MenuItem('媒体识别', icon='camera-reels'),
                sac.MenuItem('内容助手', icon='robot'),
                sac.MenuItem('字幕翻译', icon='file-earmark-break'),
                #sac.MenuItem('JupyterLab', icon='file-earmark-break'),
            ]),
        ],
        key='menu',
        open_index=[1]
    )
    sac.divider(label='POWERED BY @o3sky', icon="lightning-charge", align='center', color='gray')

    # # 添加下载模型的按钮
    # if st.button("下载模型"):
    #     # 要求用户输入密码
    #     password = st.text_input("请输入密码", type="password")
    #
    #     if password == CORRECT_PASSWORD:
    #         def download_models_and_run_command():
    #             try:
    #                 # 下载模型
    #                 snapshot_download(repo_id="hf-models/whisper-medium", local_dir="/home/app/model/whisper-medium")
    #                 snapshot_download(repo_id="hf-models/Qwen2-7B-Instruct", local_dir="/home/app/model/Qwen2-7B-Instruct")
    #                 st.success("模型下载完成！")
    #
    #                 # # 执行命令
    #                 run_vllm_model()
    #
    #
    #             except Exception as e:
    #                 st.error(f"操作失败: {e}")
    #
    #         download_models_and_run_command()
    #     else:
    #         st.error("密码错误，请重试。")

    #     # 添加 JupyterLab 链接按钮
    # if st.button("打开 JupyterLab"):
    #     st.markdown(f'<iframe src="{JUPYTER_LAB_URL}" width="100%" height="600" frameborder="0"></iframe>', unsafe_allow_html=True)
if menu == "主页":
    home()
elif menu == '内容助手':
    content()
elif menu == '媒体识别':
    media()
elif menu == '字幕翻译':
    translation()
# elif menu == 'JupyterLab':
#     st.title("JupyterLab")
#     st.markdown(f'<iframe src="{JUPYTER_LAB_URL}" width="100%" height="600" frameborder="0"></iframe>', unsafe_allow_html=True)