FROM registry.gitee-ai.local/base/iluvatar-corex:3.2.0-bi100

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y python3.10 \
    python3-pip \
    git \
    git-lfs \
    wget \
    fonts-noto-cjk

# 设置工作目录
WORKDIR /home/app

# 复制要求文件到容器中
COPY . /home/app
#RUN rm -rf /home/app/o3sky-VTAI1/model

# 安装依赖包 国内走清华源加速依赖包下载
RUN pip3 install --default-timeout=100 --no-cache-dir -r requirement/requirements-gpgpu.txt -i https://pypi.tuna.tsinghua.edu.cn/simple/
RUN pip3 install --default-timeout=100 --no-cache-dir JPype1 ipywidgets jupyterlab pandas jupyterlab-language-pack-zh-CN jupyter-server-proxy numpy seaborn scipy matplotlib pyNetLogo SALib  -i https://mirrors.cloud.tencent.com/pypi/simple
# 安装 FFmpeg
COPY /ffmpeg/ffmpeg-release-amd64-static.tar.xz /tmp/
RUN tar -xJf /tmp/ffmpeg-release-amd64-static.tar.xz -C /tmp && \
    mv /tmp/ffmpeg-*-amd64-static/ffmpeg /usr/local/bin/ && \
    mv /tmp/ffmpeg-*-amd64-static/ffprobe /usr/local/bin/ && \
    chmod +x /usr/local/bin/ffmpeg && \
    chmod +x /usr/local/bin/ffprobe && \
    #chmod +x /home/app/ffmpeg/npc && \
    rm -rf /tmp/ffmpeg-*-amd64-static /tmp/ffmpeg-release-amd64-static.tar.xz

# 设置 FFmpeg 环境变量
ENV PATH="/usr/local/bin:${PATH}"

# 下载LLM模型
#RUN git clone https://gitee.com/hf-models/faster-whisper-medium.git --depth=1 --single-branch --progress --verbose -b main  /home/app/model/medium
#RUN git clone https://gitee.com/hf-models/Qwen2-1.5B-Instruct.git --depth=1 --single-branch --progress --verbose -b main /home/app/model/Qwen2-1.5B-Instruct

# 创建非 root 用户
RUN useradd -ms /bin/bash appuser

# 更改目录所有权
RUN chown -R appuser:appuser /home/app

# 设置启动脚本权限
RUN chmod +x /home/app/start_services.sh

# 切换到非 root 用户
USER appuser

# 暴露端口
EXPOSE 7860


USER root
RUN chown -R appuser:appuser /home

# 运行命令，启动应用
#ENTRYPOINT ["streamlit", "run", "app.py", "--server.port=7860", "--server.enableCORS=false", "--server.headless=true", "--server.enableXsrfProtection=false", "--server.fileWatcherType=none", "--global.developmentMode=false", "--server.enableWebsocketCompression=false", "--browser.gatherUsageStats=false"]
ENTRYPOINT ["/home/app/start_services.sh"]