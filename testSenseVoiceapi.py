import requests

url = "http://localhost:8001/transcribe"

audio_input = {
    "file_path": "E:\\娱乐\\音频\\无心出来过的声音\\无心声音去噪.WAV",
    "device": "cuda:0",
    "model_dir": "D:\\develop\\code\\o3sky-vtai-bi100\\model\\SenseVoiceSmall",
    "vad_model": "fsmn-vad"
}

response = requests.get(url, json=audio_input)

if response.status_code == 200:
    result = response.json()
    print(result["text"])
else:
    print("Error:", response.status_code, response.text)