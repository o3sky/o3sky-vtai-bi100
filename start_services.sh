#!/bin/bash

# 启动 vllm.entrypoints.openai.api_server 单卡启动阿里通义模型
#python -m vllm.entrypoints.openai.api_server --model /data/model/Qwen2-7B-Instruct --host 127.0.0.1 --port 8000 --trust-remote-code &
# 启动 vllm.entrypoints.openai.api_server 双卡启动书生浦语模型
# 20240914修改改成使用代码启动
#python -m vllm.entrypoints.openai.api_server --model /data/model/internlm2_5-7b-chat --host 127.0.0.1 --port 8000 --trust-remote-code --tensor-parallel-size 2 &
# 启动 vllmapi.py
python /home/app/project/utils/vllmapi.py &
# 启动 SenseVoiceapi.py
python /home/app/project/utils/SenseVoiceapi.py &
# 启动 streamlit 应用
streamlit run --server.port=7860 --server.enableCORS=false --server.headless=true --server.enableXsrfProtection=false --server.fileWatcherType=none  --global.developmentMode=false  --server.enableWebsocketCompression=false --browser.gatherUsageStats=false app.py &
jupyter lab --allow-root --ip=0.0.0.0 --port=7861 --no-browser --NotebookApp.token= --NotebookApp.password= --NotebookApp.allow_remote_access=True --NotebookApp.allow_origin=* --ServerApp.disable_check_xsrf=True