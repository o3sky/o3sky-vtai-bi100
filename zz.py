import whisper
import time
def openai_whisper_model_result(file_path, device, model_name,model_path,prompt, temp, vad, lang, beam_size, min_vad):
    start_time = time.time()  # 记录开始时间
    if model_name not in ['tiny', 'tiny.en', 'base', 'base.en', 'small', 'small.en', 'medium', 'medium.en', 'large-v1',
                          'large-v2', 'large-v3', 'large']:
        print("\n*** Faster Whisper 本地模型加载模式 ***\n")
    else:
        print("\n*** Faster Whisper 调用模式 ***\n")
    print(f"- 运行模型：{model_name}")
    print(f"- 运行模型路径：{model_path}")
    print(f"- 运行方式：{device}")
    print(f"- VAD辅助：{vad}")

    try:
        file_path.split('.')
        file_path = open(file_path, "rb")
    except:
        file_path = open(file_path + "/output.mp3", "rb")
    # 加载音频文件为 NumPy 数组
    #audio = whisper.load_audio(file_path)
    model = whisper.load_model(model_name,device,model_path,in_memory=True)
    #result = model.transcribe(audio,initial_prompt=prompt,beam_size=beam_size,temperature=temp)
    result = model.transcribe("D:\\tmp\\output.mp3",initial_prompt=prompt)
    print(f"- whisper识别内容：\n{result['segments']}\n")
    # 获取 segments
    segments = result["segments"]
    segments_dict =openai_whisper_result_dict(segments)
    print(f"- whisper2识别内容：\n{segments_dict}\n")
    end_time = time.time()  # 记录结束时间
    execution_time = end_time - start_time
    print(f"- 方法执行时间：{execution_time:.2f} 秒")
    return segments_dict
def openai_whisper_result_dict(segments):
    segments_dict = {
        'text': ' '.join([segment['text'] for segment in segments]),
        'segments': [{
            'id': segment['id'],
            'seek': segment['seek'],
            'start': segment['start'],
            'end': segment['end'],
            'text': segment['text'],
            'tokens': segment['tokens'],
            'temperature': segment.get('temperature', None),
            'avg_logprob': segment['avg_logprob'],
            'compression_ratio': segment['compression_ratio'],
            'no_speech_prob': segment['no_speech_prob']}
            for segment in segments
        ]
    }
    return segments_dict
openai_whisper_model_result("D:\\tmp","cuda","medium","E:\\AI\\whisper-medium","Don’t make each line too long.","0.8",False,"自动识别",5,500)