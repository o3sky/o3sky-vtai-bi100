FROM registry.gitee-ai.local/base/iluvatar-corex:3.2.0-bi100

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y python3.10 \
    python3-pip \
    git \
    git-lfs \
    wget

# 设置工作目录
WORKDIR /home/app

# 复制要求文件到容器中
COPY . /home/app

# 安装依赖包 国内走清华源加速依赖包下载
RUN pip3 install --default-timeout=100 --no-cache-dir -r requirement/requirements-gpgpu.txt -i https://pypi.tuna.tsinghua.edu.cn/simple/

# 安装 FFmpeg
COPY /ffmpeg/ffmpeg-release-amd64-static.tar.xz /tmp/
RUN tar -xJf /tmp/ffmpeg-release-amd64-static.tar.xz -C /tmp && \
    mv /tmp/ffmpeg-*-amd64-static/ffmpeg /usr/local/bin/ && \
    mv /tmp/ffmpeg-*-amd64-static/ffprobe /usr/local/bin/ && \
    chmod +x /usr/local/bin/ffmpeg && \
    chmod +x /usr/local/bin/ffprobe && \
    rm -rf /tmp/ffmpeg-*-amd64-static /tmp/ffmpeg-release-amd64-static.tar.xz

# 设置 FFmpeg 环境变量
ENV PATH="/usr/local/bin:${PATH}"

# 创建非 root 用户
RUN useradd -ms /bin/bash appuser

# 更改目录所有权
RUN chown -R appuser:appuser /home/app

# 切换到非 root 用户
USER appuser

# 暴露端口
EXPOSE 7860

# 运行命令，启动应用
ENTRYPOINT ["streamlit", "run", "--server.port=7860", "app.py"]
USER root
RUN chown -R appuser:appuser /home